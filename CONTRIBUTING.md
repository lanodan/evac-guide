# Help Make this Guide Better

Before you participate in or contribute to this project, it is important you understand [the principles, mission and tone of UpEnd.org](https://gitlab.com/upend/upend.org-content/blob/master/raison-d-etre.md). Please make sure your additions and edits do not contradict them. If you don't agree with them, and want to say so, please debate us [on Twitter](https://twitter.com/upend_org) or any other open forum, but not here. This is a space for like minds working on a common vision. If you disregard this request, your account will be flagged and your comment or contributions deleted.





## Reporting an issue

You can see a list of issues others have reported [here](https://gitlab.com/upend/github/evac-guide/issues).  Use the `New Issue` button to report a new one. If you can, please first see if someone else has already reported your issue.





## Editing the Guide



### use WIP warnings

warn readers when the info is incomplete or not yet verified. 



### create cross-reference links

Use Markdown syntax to create direct relative links to other parts of the Guide:

- to another file, e.g.:

    `[score card](open-source-vcs-score-card.md)`

- to a heading in the same file, e.g.:

    `[migrating issues](#migrating-issues)`  will link to the heading "Migrating Issues" 

- to a heading in another file, e.g.: 

    `[migrating issues](migrating-your-repos.md#migrating-issues)` will link to the heading "Migrating Issues" within `migrating-your-repos.md`

If the links work on GitLab, it will also work in the final HTML published to UpEnd.org.



### articulate succinctly

The maintainer of this repo is far too wordy! If you can improve the Guide by saying the same thing in fewer words, please do.



### use the name "Microsoft GitHub"

Even though ownership has not yet transferred, and even though they probably won't call it "Microsoft GitHub" to bury the truth, we will call out this reality.

Microsoft apologists and GitHub employees may object. It's not a misrepresentation. So what are they afraid of? The more they don't like it, the more it proves what we believe: the acquisition stinks. 





