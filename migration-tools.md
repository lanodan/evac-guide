# Evacuation Tools

> ### TODO
>
> write about tools that can aid in migration away from GitHub.
>
> - Only include tools worthy of consideration
> - If a tool is worthy but still under developement, include it in the underdevelopment section and suggest that people help the project (link to the projects issues, reach out to the maintainer to ask what they need)
> - checkout tools at: https://github.com/topics/github-issues-export
> - checkout tools at: https://github.com/topics/github-export
>
>
> We need to actually test these tools. If we haven't, that needs to be highlighted.





## Tools we think are worth considering





## Tools we are evaluating

We have not vetted each of these yet. When we do, we'll add more info here. 



> ### TODO
>
> - check out the projects below, though maybe skip the ones that haven't been touched in over two years.
>
> - reach out to these developers.  "I can give your project exposure. But we need X"
>
> ##### Projects
>
> https://addyosmani.com/blog/backing-up-a-github-account/
> some promising tools listed here.  contact joeyh about his tool, why it is gone. it looks perfect. Note that joeyh wrote this article: https://news.ycombinator.com/item?id=17248713
>
> https://github.com/gregswindle/github-resource-converter
> looks promising
>
> https://github.com/sshaw/export-pull-requests recently updated
>
> https://gist.github.com/rodw/3073987 recently updated
>
> https://github.com/Expium/ghi2jira
>
> https://github.com/gizur/github-issues-export
>
> https://github.com/Tallwave/github_issue_exporter
>
> https://github.com/kbsbroad/github-issues-clone
>







## Developing New Export-Importers

> ### TODO
>
> - If there are no really good migration tool, we should consider initiating/sponsoring development of one.
>
> - maybe solicit developers: "We think there is need for the following tools. If you know of one that exists, let us know. If you are interested in helping develop them, let us know." or "We'd like to start a project to implement a tools that exports issues into a generic MSSSQL database or JSON format, which can then be read by importers into other systems."









