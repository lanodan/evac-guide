> ## NOTICE: Work in Progress
>
> This scorecard is a work in progress. We will remove this notice when we feel the scorecard and the detailed evaluations are reliable and complete enough for you to use for decisions. For now take it with a grain of salt.
>
> When ready, it will be published at www.upend.org/github.
>
> **Want to make this guide better or more complete?** If you any feedback or ideas, [let us know](CONTRIBUTING.md#report-an-issue) or [make a change directly](CONTRIBUTING.md#edit-the-guide).



# Open Source VCS Scorecard

> ### Example scorecards
>
> To get an idea what we are going for, check these out:
>
> https://www.eff.org/encrypt-the-web-report
>
> https://www.eff.org/who-has-your-back-2017 (notice how below the scorecard they have details for each. we will do that for now on separate pages for each option in the `options` folder, though maybe we'll decide later to put it all on one page as the EFF did.
>
> https://www.eff.org/who-has-your-back-2018
>
> The key is honesty. For example, the EFF's honesty meant they recinded their scorecard for secure messenging: https://www.eff.org/deeplinks/2018/03/secure-messaging-more-secure-mess and  https://www.dailydot.com/layer8/eff-secure-messaging-scorecard-critics/

</br>

> ### TODO: 
>
> - make tables corresponding to evaluation-criteria.md
>     - make one table for each major criteria category
>     - columns are the criteria in each category
>     - rows for each option
> - populate tables with options
> - link to the page we have for each option
> - is the "Open Source VCS" the best name for this?
> - For options still under evaluation, put a note in the row, and a link to the Issue where it is being discussed.





## Minimum Qualifications

> ### TODO
>
> state minimum requirements to be included in Guide





## Ratings

| rating | meaning                                                     |
| ------ | ----------------------------------------------------------- |
| ⭐️⭐️⭐️    | ideal or close                                              |
| ⭐️⭐️     | the closest to ideal that's (working, ready for prime time) |
| ⭐️      | better than the dominant status quo                         |
| ❌      | No. Just no.                                                |





## principles / values



|             | users first | anti-concentration of power | no advertising | commitment to social good |
| ----------- | ----------- | --------------------------- | -------------- | ------------------------- |
| GitHub      | ❌           | ❌                           | ⭐️⭐️⭐️            | ?                         |
| GitLab      | ⭐️⭐️          | ⭐️                           | ⭐️⭐️⭐️            | ?                         |
| Gitea       |             |                             |                |                           |
| Scuttlebutt |             |                             |                |                           |
|             |             |                             |                |                           |
|             |             |                             |                |                           |
|             |             |                             |                |                           |
|             |             |                             |                |                           |
|             |             |                             |                |                           |



## features

> ### TODO
>
> these categories aren't right. they are just examples. 



|             | source code mgmt | issues | pull requests | community |
| ----------- | ---------------- | ------ | ------------- | --------- |
| GitHub      |                  |        |               |           |
| GitLab      |                  |        |               |           |
| Gitea       |                  |        |               |           |
| Scuttlebutt |                  |        |               |           |
|             |                  |        |               |           |
|             |                  |        |               |           |
|             |                  |        |               |           |
|             |                  |        |               |           |
|             |                  |        |               |           |



## pragmatism



|             | works today? | if not, when? | track record | easy for your contributors to follow? |
| ----------- | ------------ | ------------- | ------------ | ------------------------------------- |
| GitHub      |              |               |              |                                       |
| GitLab      |              |               |              |                                       |
| Gitea       |              |               |              |                                       |
| Scuttlebutt |              |               |              |                                       |
|             |              |               |              |                                       |
|             |              |               |              |                                       |
|             |              |               |              |                                       |
|             |              |               |              |                                       |
|             |              |               |              |                                       |

